import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginobjectService {

  constructor() { }
}
export interface LoginObject {
  email:string;
  password:string;
}
