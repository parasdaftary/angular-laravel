import { TestBed } from '@angular/core/testing';

import { LoginobjectService } from './loginobject.service';

describe('LoginobjectService', () => {
  let service: LoginobjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginobjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
