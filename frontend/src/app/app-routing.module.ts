import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RequestResetComponent } from './components/password/request-reset/request-reset.component';
import { ResponseResetComponent } from './components/password/response-reset/response-reset.component';
import { BeforeUserloginService } from './services/before-userlogin.service';
import { AfterUserloginService } from './services/after-userlogin.service';



const appRoutes: Routes = [

  {
    path:'login',
    component:LoginComponent,
    canActivate: [BeforeUserloginService]
  },
  {
    path:'signup',
    component:SignupComponent,
    canActivate: [BeforeUserloginService]
  },
  {
    path:'profile',
    component:ProfileComponent,
    canActivate: [AfterUserloginService]
  },
  {
    path:'request-password-reset',
    component:RequestResetComponent
  },
  {
    path:'response-password-reset',
    component:ResponseResetComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
