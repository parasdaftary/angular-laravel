import { Component, OnInit } from '@angular/core';

import {LoginObject} from '/var/www/angular-laravel/frontend/src/app/loginobject.service';
import { ApiService } from 'src/app/services/api.service';
import { JwttokenService } from 'src/app/services/jwttoken.service';
import { Router } from '@angular/router';
import { AuthstatausService } from 'src/app/services/authstataus.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {

  
  
  constructor(private API:ApiService,
    private JwtToken:JwttokenService,
    private navigate : Router,
    private authStatus : AuthstatausService) { }
  
  public form = {
    email:null,
    password:null,
    name:null,
    password_confirmation:null
  };



  public loginObj: LoginObject = {
    email:null,
    password:null
  };
 
  
  onSubmit(){

    this.API.register(this.form).subscribe(
      (data) => this.handleRegister(data),
      loginObj =>  this.handleError(loginObj)
     
    ) 
    }

    handleRegister(data){

    
      this.JwtToken.handleToken(data.access_token);
      this.JwtToken.setUsername(data.user);
      this.authStatus.changeStatus(true);
      this.navigate.navigateByUrl('/profile');
    
    }

    handleError(error) {
      this.loginObj = error.error.errors;
    }

    
  ngOnInit(): void {
  }

}