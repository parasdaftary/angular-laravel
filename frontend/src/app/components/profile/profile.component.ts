import { Component, OnInit,Input } from '@angular/core';
import { JwttokenService } from 'src/app/services/jwttoken.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @Input() notifications: any[]
  public tokenItem = this.get();
  
  constructor( private JwtToken:JwttokenService) { }
  get(){
   return this.JwtToken.getUsername();
  }
  ngOnInit(): void {
  }
 
}
