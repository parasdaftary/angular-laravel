import { Component, OnInit } from '@angular/core';

import { ApiService } from 'src/app/services/api.service';
import { JwttokenService } from 'src/app/services/jwttoken.service';
import { Router } from '@angular/router';
import { AuthstatausService } from 'src/app/services/authstataus.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = {
    email:null,
    password:null
  };
  public datauser = null;
  notifications = [

    { id: 1, message: this.datauser }

   

  ];



  public error = null;
  constructor(private API:ApiService,
    private JwtToken:JwttokenService,
    private navigate : Router,
    private authStatus : AuthstatausService) { }

  onSubmit(){

this.API.login(this.form).subscribe(
  data => this.handleLogin(data),
  error => this.handleError(error)
) 
}

handleLogin(data){

  this.JwtToken.handleToken(data.access_token);
  this.JwtToken.setUsername(data.user);
  console.log(data);
  this.authStatus.changeStatus(true);
  this.datauser = data.access_token;
  console.log(this.datauser);
  this.navigate.navigateByUrl('/profile');

}

handleError(error){
  this.error = error.error.error;
}

  ngOnInit(): void {
  }

}
