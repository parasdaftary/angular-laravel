import { Component, OnInit } from '@angular/core';
import { AuthstatausService } from 'src/app/services/authstataus.service';
import { JwttokenService } from 'src/app/services/jwttoken.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private authStatus:AuthstatausService,
    private jwttoken:JwttokenService,
    private navigate:Router) { }

public userloggedIn : boolean;

  ngOnInit(){
    this.authStatus.authStatus.subscribe(value=>this.userloggedIn = value);
    
  }

  logout(event:MouseEvent){
    event.preventDefault();
    this.jwttoken.remove();
    this.authStatus.changeStatus(false);
    this.navigate.navigateByUrl("/login");
    
   
  }

}
