import { TestBed } from '@angular/core/testing';

import { AfterUserloginService } from './after-userlogin.service';

describe('AfterUserloginService', () => {
  let service: AfterUserloginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AfterUserloginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
