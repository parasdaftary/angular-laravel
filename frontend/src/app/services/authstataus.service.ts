import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { JwttokenService } from './jwttoken.service';

@Injectable({
  providedIn: 'root'
})
export class AuthstatausService {
private userIsLoggedIn = new BehaviorSubject<boolean>(this.JwtToken.loggedIn());

authStatus = this.userIsLoggedIn.asObservable();

changeStatus(value:boolean){
this.userIsLoggedIn.next(value)
}

  constructor(private JwtToken:JwttokenService) { }
}
