import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  private apiUrl = 'http://localhost:8000/api';

  register(data){
    return this.http.post(`${this.apiUrl}/signup`,data)
       }

       login(data){
        return this.http.post(`${this.apiUrl}/login`,data)
           }
       
}
