import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { JwttokenService } from './jwttoken.service';

@Injectable({
  providedIn: 'root'
})
export class AfterUserloginService implements CanActivate{

  canActivate(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): boolean 
  | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | 
  Promise<boolean | import("@angular/router").UrlTree> {
    return this.Token.loggedIn();
  }
  constructor(private Token: JwttokenService) { }
 
}
