import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { JwttokenService } from './jwttoken.service';

@Injectable({
  providedIn: 'root'
})
export class BeforeUserloginService implements CanActivate{

  constructor(private Token: JwttokenService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    return !this.Token.loggedIn();
  }
}
