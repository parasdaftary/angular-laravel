import { TestBed } from '@angular/core/testing';

import { AuthstatausService } from './authstataus.service';

describe('AuthstatausService', () => {
  let service: AuthstatausService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthstatausService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
