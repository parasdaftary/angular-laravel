# angular-laravel

Technology stack

Angular 
Laravel REST API
Bootstrap
JWT based authentication


<pre><font color="#8AE234"><b>paras@paras-VirtualBox</b></font>:<font color="#729FCF"><b>/var/www/angular-laravel</b></font>$ ng --version

<font color="#CC0000">     _                      _                 ____ _     ___</font>
<font color="#CC0000">    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|</font>
<font color="#CC0000">   / △ \ | &apos;_ \ / _` | | | | |/ _` | &apos;__|   | |   | |    | |</font>
<font color="#CC0000">  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |</font>
<font color="#CC0000"> /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|</font>
<font color="#CC0000">                |___/</font>
<font color="#CC0000">    </font>

Angular CLI: 9.0.3
Node: 10.19.0
OS: linux x64

Angular: 
... 
Ivy Workspace: 

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.900.3
@angular-devkit/core         9.0.3
@angular-devkit/schematics   9.0.3
@schematics/angular          9.0.3
@schematics/update           0.900.3
rxjs                         6.5.3
</pre>                       

<pre><font color="#8AE234"><b>paras@paras-VirtualBox</b></font>:<font color="#729FCF"><b>/var/www/angular-laravel</b></font>$ npm --version
6.13.4
</pre>

<pre><font color="#8AE234"><b>paras@paras-VirtualBox</b></font>:<font color="#729FCF"><b>/var/www/angular-laravel</b></font>$ node -v
v10.19.0
</pre>

<pre><font color="#8AE234"><b>paras@paras-VirtualBox</b></font>:<font color="#729FCF"><b>/var/www/angular-laravel/api</b></font>$ php artisan --version
Laravel Framework <font color="#4E9A06">6.17.1</font>
</pre>
